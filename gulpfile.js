const gulp = require('gulp');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify-es').default;
const minify = require('gulp-minify-css');

gulp.task('js', (done) => {
  gulp.src([
    '*.js',
    '!gulpfile.js',
  ])
    .pipe(concat('app.js'))
    .pipe(uglify({
      mangle: {
        toplevel: true,
      },
    }))
    .pipe(gulp.dest('./dist'));
  done();
});

gulp.task('css', (done) => {
  gulp.src('*.css')
    .pipe(concat('style.css'))
    .pipe(minify())
    .pipe(gulp.dest('./dist'));
  done();
});

gulp.task('watch', () => {
  return gulp.watch([
    '*.js',
    '*.css',
    '!gulpfile.js',
  ],
    gulp.parallel('js', 'css')
  );
});

gulp.task('default', gulp.parallel('js', 'css'), (done) => {
  done();
});
