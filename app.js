class Apprenant {
  constructor(prenom, urlPhoto) {
    this.prenom = prenom;
    this.urlPhoto = urlPhoto;

    const clone = Apprenant.templateJoueurDOM.content.cloneNode(true);
    clone.querySelector('.prenom').textContent = this.prenom;
    clone.querySelector('.joueurn').id = this.prenom;
    clone.querySelector('.photo').style.backgroundImage = `url("${this.urlPhoto}")`;
    Apprenant.joueursDOM.appendChild(clone);

    this.caseDom = document.getElementById(this.prenom);

    this.caseDom.addEventListener('click', this.eliminerAbsent.bind(this));
    this.caseDom.addEventListener('keydown', (event) => {
      if (event.code === 'Space' || event.code === 'Enter')
        this.eliminerAbsent();
    });
    Apprenant.participants.push(this);
  }

  eliminerAbsent() {
    if (this.caseDom.classList.contains('retourne') === false) {
      this.caseDom.classList.add('retourne', 'absent');
      Apprenant.participants = Apprenant.participants.filter(
        (participant) => participant.prenom !== this.prenom
      );
      Apprenant.sounds.fatality.load();
      Apprenant.sounds.fatality.play();
    }
  }
}
// Les propriétés statiques n'ont pas encore été implémentées en JS.
// Je définis la propriété à l'extérieur de ma classe.
Apprenant.participants = [];
Apprenant.sounds = {
  fatality: new Audio('./sounds/fatality.mp3'),
}
Apprenant.joueursDOM = document.getElementById('joueurs');
Apprenant.templateJoueurDOM = document.getElementById('templateJoueurDOM');

class Role {
  constructor(caseDom, message) {
    this.caseDom = caseDom;
    this.message = message;
    this.participant = undefined;
  }

  tirageAleatoire() {
    if (Apprenant.participants.length === 0) return;
    const participantChoisi = Math.floor(Math.random() * Apprenant.participants.length);
    this.participant = Apprenant.participants.splice(participantChoisi, 1)[0];
    this.caseDom.querySelector('.contentCase').textContent = this.participant.prenom;
    this.caseDom.querySelector('.photo').style.backgroundImage = `url(${this.participant.urlPhoto})`;
    this.participant.caseDom.classList.add('retourne');
    Role.speak(`${this.participant.prenom}, ${this.message}`);
  }

  switchRole(role) {
    this.participant = role.participant;
    this.caseDom.querySelector('.contentCase').textContent = role.participant.prenom;
    this.caseDom.querySelector('.photo').style.backgroundImage = `url(${role.participant.urlPhoto})`;
    Role.speak(`${this.participant.prenom}, ${this.message}`);
  }

  static speak(string) {
    const speech = new SpeechSynthesisUtterance(string);
    window.speechSynthesis.speak(speech);
  }

  static changerUkeTori() {
    uke.switchRole(tori);
    tori.switchRole(suivant);
    suivant.caseDom.querySelector('.contentCase').textContent = '';
    suivant.caseDom.querySelector('.photo').style.backgroundImage = '';
  }
}

const tori = new Role(document.getElementById('tori'), 'tu donnes les instructions');
const uke = new Role(document.getElementById('uke'), 'tu vas au clavier');
const suivant = new Role(document.getElementById('suivant'), "c'est bientôt à toi");

class Chrono {
  constructor() {
    this.secondesInitiales = 300;
    this.secondes = this.secondesInitiales;
    this.speed = 1000; // ms
    this.running = false;
    this.dernierTour = false;
    this.interval;
    this.compteurSecondes = document.getElementById('compteur');
    this.barreProgression = document.getElementById('prog');
    this.boutons = {
      ukeTori: document.getElementById('ukeTori'),
      lancer: document.getElementById('lancer'),
      pause: document.getElementById('pause'),
      stop: document.getElementById('stop'),
    };

    this.boutons.lancer.disabled = true;
    this.boutons.pause.disabled = true;
    this.boutons.stop.disabled = true;
    this.boutons.ukeTori.addEventListener('click', this.premierTirage.bind(this));
    this.boutons.lancer.addEventListener('click', this.lancer.bind(this));
    this.boutons.pause.addEventListener('click', this.pause.bind(this));
    this.boutons.stop.addEventListener('click', this.stop.bind(this));
    this.formaterTexte()
  }

  premierTirage() {
    this.boutons.ukeTori.disabled = true;
    this.boutons.lancer.disabled = false;
    this.boutons.pause.disabled = false;
    this.boutons.stop.disabled = false;
    tori.tirageAleatoire();
    uke.tirageAleatoire();
  }

  lancer() {
    if (this.running === false) {
      this.running = true;
      this.interval = setInterval(this.retirerSeconde.bind(this), this.speed);
    }
  }

  pause() {
    this.running = false;
    clearInterval(this.interval);
  }

  stop() {
    this.running = false;
    clearInterval(this.interval);
    this.resetBarreProgression();
  }

  retirerSeconde() {
    this.secondes -= 1;
    const pourcentage = (100 * this.secondes) / this.secondesInitiales;
    const secondesRestantes = {
      60: () => {
        this.barreProgression.classList.add('peuDeTemps');
        suivant.tirageAleatoire();
        Chrono.sounds.ting.play();
      },
      15: () => this.barreProgression.classList.add('stress'),
      0: () => this.chronoZero(),
    }
    secondesRestantes[this.secondes] && secondesRestantes[this.secondes]();
    this.barreProgression.style.setProperty('--barre-width', pourcentage);
    this.formaterTexte();
  }

  formaterTexte() {
    const minutesFormate = Math.floor(this.secondes / 60);
    const secondesFormate = (this.secondes % 60).toString().padStart(2, '0');
    this.compteurSecondes.textContent = `${minutesFormate}:${secondesFormate}`;
  }

  resetBarreProgression() {
    this.secondes = this.secondesInitiales;
    this.formaterTexte();
    this.barreProgression.style.setProperty('--barre-width', 100);
    this.barreProgression.classList.remove('peuDeTemps', 'stress');
  }

  chronoZero() {
    if (this.dernierTour) {
      clearInterval(this.interval);
      this.boutons.lancer.disabled = true;
      this.boutons.pause.disabled = true;
      this.boutons.stop.disabled = true;
      this.running = false;
      return;
    }
    this.resetBarreProgression();
    Role.changerUkeTori();
    Chrono.sounds.buzz.play();
    if (Apprenant.participants.length === 0) this.dernierTour = true;
  }
}

Chrono.sounds = {
  ting: new Audio('./sounds/ting.wav'),
  buzz: new Audio('./sounds/buzz.wav'),
}

new Chrono();

(async function genererAppenants() {
  const promo = await fetch('./promo.json').then((res) => res.json());
  promo.forEach((apprenant) => new Apprenant(apprenant.prenom, apprenant.urlPhoto));
}());
